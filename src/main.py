import json
import requests
#import database
import sqlite3
import pprint
from datetime import datetime

#skaffa en request till hemsidan
r = requests.get('https://jobb.bravura.se/api/job/JobFilter?CategoryIds=558201&CityIds=541401&Internal=false&SearchWords=')

#Gör requesten till ett json-object
jsonObject = r.json()

#Visa json-keysen
x = jsonObject.keys()


#connectar en DB med sqlite3 som heter databse.db där vi tar info från json_data.json som vi läser all info in till dbn.
db = sqlite3.connect('database.db')
with open('json_data.json', encoding='utf-8-sig') as json_file:
    json_data = json.loads(json_file.read())

length_of_jobs = len(jsonObject["Jobs"])


#Skapar våra tabeller, dessa tabeller är då skapade från json_data.json filen
columns = (("Category", "TEXT"),
            ("City", "TEXT"),
            ("CompanyName", "TEXT"),
            ("Extent", "TEXT"),
            ("Id", "INTEGER PRIMARY KEY AUTOINCREMENT"),
            ("JobType", "TEXT"),
            ("Name", "TEXT"),
            ("PublishDate", "DATETIME"),
            ("Slug", "TEXT"))

#Vår funktion som tar "Jobs" och appendar de in till tabellerna.
def connect_database():
    #Extraherar i JSON-filen och i detta falet "Jobs" och loopar igenom och drar ut all info till en lista.
    raw_jobs = json_data["Jobs"]
    flat_jobs = []
    for job in raw_jobs:
        flat_job = []

        flat_job.append(job["Category"]["Name"])
        flat_job.append(job["City"]["Name"])
        flat_job.append(job["CompanyName"])
        flat_job.append(job["Extent"]["Name"])
        flat_job.append(job["Id"])
        flat_job.append(job["JobType"]["Name"])
        flat_job.append(job["Name"])
        flat_job.append(job["PublishDate"])
        flat_job.append(job["Slug"])

        flat_jobs.append(flat_job)

#Dags att generera och skapa och trycka in queries och skicka det in i sqlite3 databasen
    create_query = "create table if not exists jobs ({0})".format(", ".join([f"{c[0]} {c[1]}" for c in columns]))
    insert_query = "insert into jobs ({0}) values ({1})".format(", ".join([c[0] for c in columns]), ", ".join(("?" for _ in range(len(columns)))))
    print("insert has started at " + str(datetime.now()))
    c = db.cursor()
    c.execute(create_query)
    c.executemany(insert_query,flat_jobs)
    # values.clear()
    db.commit()
    c.close()
    print("insert has completed at " + str(datetime.now()))


#GUI för själva appen
MENU_PROMPT = """-- Jobs from bravura -- \n

# Välj ett av dessa alternativ:

# 1) Addera in databasen.
# 2) Visa hur många IT-jobb det finns Stockholm från jobb.bravura.se.
# 3) Hitta ett jobb i db.
# 4) Extrahera alla jobs till en JSON-fil.
# 5) Exit.

# Ditt val: """

def menu():
    while (user_input := input(MENU_PROMPT)) != "5":
        #Skapar vår databas
        if user_input == "1":
            connect_database()

        #Visar hur många IT-jobb det finns i Stockholm ute på jobb.bravura.se
        elif user_input == "2":
            print(f"Det finns såhär många IT-jobb i Stockholm:{length_of_jobs}")

         #Hitta ett jobb i databasen
        elif user_input == "3":

            print()

        #Extraherar till en JSON-fil
        elif user_input == "4":
            with open('json_data.json', 'w') as outfile:
                json.dump(jsonObject, outfile, indent=4, sort_keys=True)
        else:
            print("Invalid user input")
            menu()

menu()


#--------------------------Saker att strunta i-------------------------

#Syftet med detta block är att få listan över kolumner i JSON-filen.
    # columns = [key for key in json_data if key not in columns]
# columns = []
# for data in json_data:
#     if data not in columns:
#         columns.append(data)

# #värden på kolumnerna i JSON-filen i rätt ordning.
# values = []
# for column in columns:
#     values.append(json_data[column])
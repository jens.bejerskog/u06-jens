FROM python:3.9

WORKDIR /src

COPY . .

COPY requirements.txt .
RUN pip install -r requirements.txt

CMD ["python", "src/main.py"]